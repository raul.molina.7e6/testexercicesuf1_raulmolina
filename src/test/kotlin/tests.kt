import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2_TestKtTest{
    @Test
    fun multiplicaPorDos(){
        assertEquals(4, multiplicador(2))
    }
    @Test
    fun negativeNumber(){
        assertEquals(-4, multiplicador(-2))
    }
    @Test
    fun bigNumber(){
        assertEquals(6000, multiplicador(3000))
    }
}

internal class _1_3_TestKtTest{
    @Test
    fun sumaSimple(){
        assertEquals(5, suma(2,3))
    }
    @Test
    fun sumaGrande(){
        assertEquals(7000, suma(3000,4000))
    }
    @Test
    fun negativeNumber(){
        assertEquals(5, suma(-7,12))
    }
}

internal class _1_4_TestKtTest{
    @Test
    fun multSimple(){
        assertEquals(6, area(2,3))
    }
    @Test
    fun multGrande(){
        assertEquals(3741, area(87,43))
    }
    @Test
    fun negativeNumber(){
        assertEquals(-10, area(-2,5))
    }
}

internal class _1_5_TestKtTest{
    @Test
    fun resultatEsperat(){// 2 5 8 4
        assertEquals(0, calculBoig(2,5,8,4))
    }

    @Test
    fun noResultatEsperat(){//2 5 8 5
        assertNotEquals(0, calculBoig(2,5,8,5))
    }

    @Test
    fun resultatEsperat2(){// 2 5 8 5
        assertEquals(21, calculBoig(2,5,8,5))
    }
}

internal class _1_6_TestKtTest{
    @Test
    fun calculoCorrecto(){
        assertEquals(6, calculadorPupitres(3,7,2))
    }

    @Test
    fun calculoCorrecto2(){
        assertEquals(25, calculadorPupitres(23,22,5))
    }

    @Test
    fun calculoNoCorrecto(){
        assertNotEquals(1, calculadorPupitres(23,22,5))
    }
}

internal class _1_7_TestKtTest{
    @Test
    fun esElSiguiente(){
        assertEquals(1, numSiguiente(0))
    }

    @Test
    fun esElSiguiente2(){
        assertEquals(2, numSiguiente(1))
    }

    @Test
    fun noEsElSiguiente(){
        assertNotEquals(3, numSiguiente(1))
    }
}

internal class _1_8_TestKtTest{
    @Test
    fun multiplicaBe(){
        assertEquals(2.0, multiplicacio(1.0))
    }

    @Test
    fun multiplicaBe2(){
        assertEquals(5.50, multiplicacio(2.75))
    }

    @Test
    fun resultatNoEsUnDouble(){
        assertNotEquals(2, multiplicacio(1.0))
    }
}

internal class _1_9_TestKtTest{
    @Test
    fun malDescompte(){
        assertNotEquals(10.0, calculoDescuento(30.0, 20.0))
    }

    @Test
    fun malDescompte2(){
        assertNotEquals(10.0, calculoDescuento(40.0, 20.0))
    }

    @Test
    fun bonDescomte(){
        assertEquals(10.0, calculoDescuento(200.0, 180.0))
    }
}

internal class _1_10_TestKtTest{
    @Test
    fun bonCalcul(){
        assertEquals(415.4756284372501, calculoAreaPizza(23.0))
    }

    fun noBonResultat(){
        assertNotEquals(412.4756284372501, calculoAreaPizza(23.0))
    }

    fun resultatNoEsDouble(){
        assertNotEquals(30, calculoAreaPizza(3.0))
    }
}

internal class _1_11_TestKtTest{
    @Test
    fun bonCalcul(){
        assertEquals(24.0, calculVolum(2.0,3.0,4.0))
    }

    @Test
    fun bonCalcul0(){
        assertEquals(48.0, calculVolum(4.0,3.0,4.0))
    }

    @Test
    fun resultatNoEsDouble(){
        assertNotEquals(24, calculVolum(2.0,3.0,4.0))
    }
}

internal class _1_12_TestKtTest{
    @Test
    fun bonResultat(){
        assertEquals(32.0, toFarenheits(0.0))
    }

    @Test
    fun resultatNoEsDouble(){
        assertNotEquals(32, toFarenheits(0.0))
    }

    @Test
    fun malResultat(){
        assertNotEquals(31.0, toFarenheits(0.0))
    }
}

internal class _1_13_TestKtTest{
    @Test
    fun buenResultado(){
        assertEquals(5.0, augmentoDeTemperatura(2.0,3.0))
    }
    @Test
    fun buenResultado2(){
        assertEquals(10.0, augmentoDeTemperatura(7.0,3.0))
    }
    @Test
    fun malResultado(){
        assertNotEquals(5.0, augmentoDeTemperatura(3.0,3.0))
    }
}

internal class _1_14_TestKtTest{
    @Test
    fun mediaBunea(){
        assertEquals(10.0, costPerPersona(30.0,3.0))
    }

    @Test
    fun resultatNoEsDouble(){
        assertNotEquals(10, costPerPersona(50.0, 5.0))
    }

    @Test
    fun malResultado(){
        assertNotEquals(3.0, costPerPersona(30.0,3.0))
    }
}

internal class _1_15_TestKtTest{
    @Test
    fun segundoSiguiente(){
        assertEquals(4, segundosMasUno(3))
    }

    @Test
    fun segundoSiguiente2(){
        assertEquals(0, segundosMasUno(59))
    }

    @Test
    fun noSegundoSiguiente(){
        assertNotEquals(31, segundosMasUno(29))
    }
}

internal class _1_16_TestKtTest{
    @Test
    fun buenResultado(){
        assertEquals(1.0, aDouble(1))
    }

    @Test
    fun buenResultado2(){
        assertEquals(2.0, aDouble(2))
    }

    @Test
    fun noBuenResultado(){
        assertNotEquals(3, aDouble(3))
    }
}
