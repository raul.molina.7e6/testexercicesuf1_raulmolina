import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/19
* TITLE: Pupitres
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada1 = scanner.nextInt()
    val entrada2 = scanner.nextInt()
    val entrada3 = scanner.nextInt()
    println("${calculadorPupitres(entrada1, entrada2, entrada3)}")
}
fun calculadorPupitres(a: Int, b: Int, c: Int): Int{
    return (a+b+c)/2
}