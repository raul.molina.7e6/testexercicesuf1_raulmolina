import java.util.*

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/19
* TITLE: Operació boja
 */

fun main(){
    val scanner = Scanner(System.`in`)
    val a = scanner.nextInt()
    val b = scanner.nextInt()
    val c = scanner.nextInt()
    val d = scanner.nextInt()
    println("${calculBoig(a, b, c, d)}")
}

fun calculBoig(a: Int, b: Int, c:Int, d:Int): Int{
    return (a + b) * (c % d)
}



