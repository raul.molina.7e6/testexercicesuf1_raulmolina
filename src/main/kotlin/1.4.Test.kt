import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/19
* TITLE: Calcula l’àrea
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val entrada1 = scanner.nextInt()
    val entrada2 = scanner.nextInt()
    println("${area(entrada1, entrada2)}")

}
fun area(num1: Int, num2: Int): Int{
    return num1 * num2
}

