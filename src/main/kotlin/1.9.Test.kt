/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/19
* TITLE: Calcula el descompte
*/

import java.util.*
fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("introdueix el preu original:")
    val userInput = scanner.nextDouble()
    println("introdueix el preu amb el descompte:")
    val userInput2 = scanner.nextDouble()
    println("el percentatge descomptat es:")
    println("${calculoDescuento(userInput, userInput2)}")
}
fun calculoDescuento(a: Double, b:Double): Double{
    return 100-((b*100)/a)
}

