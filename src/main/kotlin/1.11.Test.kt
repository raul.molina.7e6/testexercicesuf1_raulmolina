/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/20
* TITLE: Calculadora de volum d’aire
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val llargada = scanner.nextDouble()
    val amplada = scanner.nextDouble()
    val alçada = scanner.nextDouble()
    println("${calculVolum(llargada, amplada, alçada)}")
}
fun calculVolum(a: Double, b: Double, c:Double): Double{
    return a*b*c
}