/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/20
* TITLE: De Celsius a Fahrenheit
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Escribe la temperatura en celsius: ")
    val celsius = scanner.nextDouble()
    print("la temperatura en farenheits es: ")
    println(toFarenheits(celsius))
}

fun toFarenheits (entrada: Double): Double{
    return ((entrada*1.8)+32)
}