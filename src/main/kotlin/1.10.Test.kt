/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/20
* TITLE: Quina és la mida de la meva pizza?
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Cual es el diametro de la pizza?")
    val userInput = scanner.nextDouble()
    print("El area de la pizza es de: ")
    println("${calculoAreaPizza(userInput)}")
}
fun calculoAreaPizza(a: Double): Double{
    return (a*a)*Math.PI*1/4
}

