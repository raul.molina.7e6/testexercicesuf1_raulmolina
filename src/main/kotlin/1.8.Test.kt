/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/19
* TITLE: Dobla el decimal
*/

import java.util.*
fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("introdueix un nombre:")
    val userInput = scanner.nextDouble()
    println("el doble es:")
    println("${multiplicacio(userInput)}")
}

fun multiplicacio(a: Double): Double{
    return a*2
}
