/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/19
* TITLE: Número següent
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("introdueix un nombre enter:")
    val userInput = scanner.nextInt()
    println("despres ve el ${numSiguiente(userInput)}")
}

fun numSiguiente(a: Int):Int{
    return a+1
}
