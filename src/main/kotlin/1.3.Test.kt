import java.util.Scanner

/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/19
* TITLE: Suma de dos nombres enters
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val num1 = scanner.nextInt()
    val num2 = scanner.nextInt()
    println("${suma(num1, num2)}")
}
fun suma(num1: Int, num2: Int): Int{
    return num1 + num2
}