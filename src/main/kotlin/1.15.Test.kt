/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/20
* TITLE: Afegeix un segon
* segons < 60
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Quants segons son?")
    val segons = scanner.nextInt()
    println(segundosMasUno(segons))
}
fun segundosMasUno(entrada: Int):Int{
    return (entrada+1)%60
}