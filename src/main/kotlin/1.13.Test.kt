/*
* AUTHOR: Raul Molina Kind
* DATE: 2022/09/20
* TITLE: Quina temperatura fa?
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Quina es la temperatura actual?")
    val tempAct = scanner.nextDouble()
    println("Quin es l'agument de temperatura?")
    val tempAug = scanner.nextDouble()
    print("La temperatura actual és ")
    println(augmentoDeTemperatura(tempAct, tempAug))
}

fun augmentoDeTemperatura (entrada1: Double, entrada2: Double):Double{
    return entrada1 + entrada2
}